Subversion XML Log Export to Microsoft Excel XML Converter
==========================================================

Converts Subversion log XML exports to Excel-compatible XML files for further
processing, preserving line breaks in log entries.

The generated table columns are the revision number, timestamp, committer and
text for each log entry of the processed log export.


Usage
-----

	svnlog2xls.sh [options] [file]

All arguments are optional. If no argument is provided, the resulting Excel
XML will not contain an author or company name, Subversion log XML exports are
read from standard input and the resulting Excel XML is written to standard output.

__Optional Options__

- `-a <author>` or `--author <author>`

  Optional name of author to be used in output file metadata for the author
  and last author properties.

- `-c <company>` or `--company <company>`

  Optional company name used in output file metadata for the company property.

- `-f <file>`, `--input <file>` or as stand-alone argument

  Optional file containing a Subversion log export in XML format. If omitted
  or a dash (-) is used instead, the log export is read from standard input.

- `-h` or `--help`

  Shows short tool usage instructions and exits.

- `-O <file>` or `--output <file>`

  Target filename to write Excel XML to. If omitted, the resulting Excel XML
  is written to standard output.

- `-V` or `--version`

  Shows tool version information and exits.


__Examples__

- Retrieve log for revisions 100 and 101 from repository and store resulting
  Excel XML to `out.xls`:

		svn log -r 100:101 --xml path-to-repository | svnlog2xls.sh >out.xls

- Convert existing Subversion log XML export to Excel XML and set author and
  company metadata:

		svnlog2xls.sh -a "Jon Doe" -c "ACME Inc." export.xml >out.xls

- Same as before, using long for all and explicit arguments for input and output files:

		svnlog2xls.sh --author "Jon Doe" --company "ACME Inc." --input export.xml --output out.xls


Installation
------------

1. Check out the repository to a location of your choice.
2. Install requirements as needed (see below).
3. Optional. Create a symbolic link to `svnlog2xml.sh` or add repository directory
   to your `PATH`. The symbolic link name can differ from the original tool name
   (e.g. omit the .sh extension). Symbolic links are automatically resolved.


Requirements
------------

Expects the `xsltproc` tool in `PATH`.


Legal
-----

Microsoft and Excel are either registered trademarks or trademarks of Microsoft Corporation in the United States and/or other countries.

Subversion and the Apache Subversion logo are registered trademarks of The Apache Software Foundation.


License
-------

SVNLog2XML is released under the [Apache License, Version 2.0][1].

Copyright(C) 2012, xplo.re IT Services, Michael Maier.
All rights reserved.

[1]: <http://www.apache.org/licenses/LICENSE-2.0> "Apache License 2.0"
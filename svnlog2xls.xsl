<?xml version="1.0" encoding="utf-8"?>
<!-- -*- Mode: XSL; tab-width: 4; indent-tabs-mode: on; coding: utf-8 -*- -->
<!--
    Subversion XML Log Export to MS-Excel XML Converter

    Copyright(C) 2012, xplo.re IT Services, Michael Maier.
    All rights reserved.

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
-->

<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="urn:schemas-microsoft-com:office:spreadsheet"
	xmlns:o="urn:schemas-microsoft-com:office:office"
	xmlns:x="urn:schemas-microsoft-com:office:excel"
	xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
	xmlns:html="http://www.w3.org/TR/REC-html40"
	>

	<!-- Generate XML output in standard UTF-8 encoding. -->
	<xsl:output method="xml" version="1.0" encoding="utf-8" indent="yes" />

	<!-- There are no special aligned whitespace-tags, thus strip whitespaces. -->
	<xsl:strip-space elements="*" />

	<!-- Parameters. -->
	<xsl:param name="author" />
	<xsl:param name="company" />

	<!-- Match LOG. -->
	<xsl:template match="log">
		<Workbook>
			<DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
				<Author><xsl:value-of select="$author" /></Author>
				<LastAuthor><xsl:value-of select="$author" /></LastAuthor>
				<Created><xsl:value-of select="logentry[1]/date" /></Created>
				<LastSaved><xsl:value-of select="logentry[last()]/date" /></LastSaved>
				<Company><xsl:value-of select="$company" /></Company>
				<Version><xsl:value-of select="logentry[last()]/@revision" /></Version>
			</DocumentProperties>

			<Styles>
				<Style ss:ID="Default" ss:Name="Normal">
					<Alignment ss:Vertical="Top"/>
					<Borders/>
					<Font/>
					<Interior/>
					<NumberFormat/>
					<Protection/>
				</Style>
				<Style ss:ID="DateLiteral">
					<NumberFormat ss:Format="Short Date"/>
				</Style>
				<Style ss:ID="Expand">
					<Alignment ss:Vertical="Top" ss:WrapText="1" />
				</Style>
			</Styles>

			<Worksheet ss:Name="Log">
				<Table ss:ExpandedColumnCount="4" ss:ExpandedRowCount="{count(logentry)}" x:FullColumns="1" x:FullRows="1">
					<Column ss:Width="35" />
					<Column ss:Width="60" />
					<Column ss:Width="100" />
					<Column ss:StyleID="Expand" ss:AutoFitWidth="0" ss:Width="300" />
					<xsl:apply-templates select="logentry" />
				</Table>
			</Worksheet>
		</Workbook>
	</xsl:template>

	<!-- Match LOGENTRY. -->
	<xsl:template match="logentry">
		<Row>
			<Cell><Data ss:Type="Number"><xsl:value-of select="@revision" /></Data></Cell>
			<Cell ss:StyleID="DateLiteral"><Data ss:Type="DateTime"><xsl:value-of select="date" /></Data></Cell>
			<Cell><Data ss:Type="String"><xsl:value-of select="author" /></Data></Cell>
			<Cell>
				<Data ss:Type="String">
					<xsl:call-template name="trim-right">
						<xsl:with-param name="string" select="translate(msg,'&#10;','&#13;')" />
					</xsl:call-template>
				</Data>
			</Cell>
		</Row>
	</xsl:template>

	<!-- Template trim-right(string): Trim whitespaces in string from right. -->
	<xsl:template name="trim-right">
		<xsl:param name="string" />

		<xsl:choose>
			<xsl:when test="string-length($string) = 0">
				<xsl:value-of select="$string"/>
			</xsl:when>
			<xsl:when test="normalize-space(substring($string, string-length($string))) = ''">
				<xsl:call-template name="trim-right">
					<xsl:with-param name="string" select="substring($string, 1, string-length($string) - 1)" />
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$string" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

</xsl:stylesheet>
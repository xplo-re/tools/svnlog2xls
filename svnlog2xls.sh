#!/bin/sh
# -*- Mode: Shell Script; tab-width: 4; indent-tabs-mode: on; coding: utf-8 -*-
# Subversion XML Log Export to MS-Excel XML Converter
#
# Copyright(C) 2012, xplo.re IT Services, Michael Maier.
# All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Configuration.
XSLTPROC=xsltproc

# Report an error and abort script.
die()
{
	error "$1"
	exit 1
}

# Report an error.
error()
{
	echo "$PROGRAM: error: $1" >&2
}

# Parses command line arguments. getopt is of no use, as it doesn't support
# long options on some systems (notably BSD and derived system such as Mac OS
# X), so we would have to implement parsing those anyway.
parse_arguments()
{
	while test $# -gt 0;
	do
		case "$1"
		in
			-a|--author)
				AUTHOR="$2"
				shift
				;;
			-c|--company)
				COMPANY="$2"
				shift
				;;
			-h|--help)
				usage
				exit 0
				;;
			-f|--input)
				if test -z "$INFILE";
				then
					INFILE="$2"
				else
					die "multiple input files specified"
				fi
				shift
				;;
			-O|--output)
				if test -z "$OUTFILE";
				then
					OUTFILE="$2"
				else
					die "multiple output files specified"
				fi
				shift
				;;
			-V|--version)
				version
				exit 0
				;;
			-)
				if test -z "$INFILE";
				then
					INFILE="$1"
				else
					die "cannot read from both standard input and input file"
				fi
				;;
			-*)
				die "unsupported argument '$1'"
				;;
			*)
				if test -z "$INFILE";
				then
					INFILE="$1"
				else
					die "unsupported argument '$1'"
				fi
				;;
		esac

		shift
	done
}

# Canonicalises path by resolving symbolic links.
resolve_path()
{
	local resolved="$1"
	local target

	while test -L "$resolved";
	do
		target=$(expr "`ls -ld "$resolved"`" : '.*-> \(.*\)$')

		if expr "$target" : '/.*' >/dev/null;
		then
			resolved="$target"
		else
			# Prepend path information for link within same directory.
			resolved="$(dirname "$resolved")/$target"
		fi
	done

	echo "$resolved"
}

# Print usage instructions.
usage()
{
	echo "usage: $PROGRAM [options] [file]"
	echo
	echo "  Converts Subversion log XML exports to Excel-compatible XML streams."
	echo "  If file is omitted or is a dash (-), standard input is used. Expects"
	echo "  xsltproc in PATH."
	echo
	echo "  Optional options:"
	echo "  -a <author>   Name of author to store to metadata of resulting document."
	echo "                Long option: --author"
	echo "  -c <company>  Name of company to store to metadata of resulting document."
	echo "                Long option: --company"
	echo "  -f <file>     File path to read Subversion log XML export from."
	echo "                Long option: --input"
	echo "  -h            Shows this help message and exits."
	echo "                Long option: --help"
	echo "  -O <output>   Output file path to write Excel XML to."
	echo "                Long option: --output"
	echo "  -V            Show version information."
	echo "                Long option: --version"
}

# Verify existence of tools used by the script.
verify_tools()
{
	local tool
	local toolname
	local toolpath

	for tool in XSLTPROC;
	do
		eval "toolname=\"\${$tool}\""
		toolpath=$(which "$toolname")

		if test -z "$toolpath" -o ! -x "$toolpath";
		then
			die "cannot find $tool tool (set to '$toolname')"
		fi
	done
}

# Prints out version information.
version()
{
	echo "Subversion XML Log Export to MS-Excel XML Converter v$VERSION"
	echo "Copyright(C) 2012, xplo.re IT Services, Michael Maier."
	echo "All rights reserved."
}

# Globals.
VERSION=1.0
PROGRAM=$(basename "$0")
RSRC=$(dirname "$(resolve_path "$0")")
AUTHOR=
COMPANY=
INFILE=
OUTFILE=

# Resources.
RSRCXSL="$RSRC/svnlog2xls.xsl"

# Bootstrap.
verify_tools
parse_arguments "$@"

# Perform XSLT transformation.
if test -z "$INFILE";
then
	INFILE="-"
fi

if test -z "$OUTFILE";
then
	"$XSLTPROC" --stringparam author "$AUTHOR" --stringparam company "$COMPANY" "$RSRCXSL" "$INFILE"
else
	"$XSLTPROC" --stringparam author "$AUTHOR" --stringparam company "$COMPANY" --output "$OUTFILE" "$RSRCXSL" "$INFILE"
fi
